const express = require("express");
const app = express();
const uuidV4 = require("uuid/v4");
const config = require("./config");
const port = process.env.PORT || config.Port || 9000;
const shorturl = require("./shorturl");
const visits = require("./visits");

const dbdatamanager = require("./dbdatamanager");
var dbDataManager = new dbdatamanager.DBDataManager();

app.use(function (req, res, next) {
    console.log(req.method + " " + req.originalUrl);  
    
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS");
    res.header("Access-Control-Allow-Headers", "Content-Type, Authorization, Content-Length, X-Requested-With");
    res.header("Content-Type", "application/json");
  
    if ( req.method === "OPTIONS" ) {
        return res.status(200).end();
    }  

	next();
});

app.get("/:shorturl", function(req, res) {
    var shorturl = req.params.shorturl;

    dbDataManager.selectURLByShortURL(shorturl, (result, error) => {
        if ( error ) {              
            return res.status(500).end(JSON.stringify({}));              
        }

        if (result.length == 0) {
            return res.status(404).end();
        }

        var redirectURL = result[0].url;

        var data = {
            id: uuidV4(),
            ip: req.ip,
            url: result[0].id,
            data: req.get("User-Agent")
        };
        dbDataManager.insertVisit(data, (result, error) => {
            if ( error ) {              
                return res.status(500).end(JSON.stringify({}));              
            }

            res.redirect(redirectURL);
        });        
    });    
});

app.use("/api/shorturl", shorturl);
app.use("/api/visits", visits);

app.listen(port);
console.log("Server started " + "127.0.0.1:" + port);