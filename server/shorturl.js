const express = require("express");
const router = express.Router();
const bodyParser = require("body-parser");
const uuidV4 = require("uuid/v4");

const dbdatamanager = require("./dbdatamanager");
var dbDataManager = new dbdatamanager.DBDataManager();

const base62 = require('./base62.js');

function uuidBufferToString(data) {
	var outString = "";
	for ( var i = 0; i < data.length; i++ ) {
		var c = data[i];
		outString += c.toString(16);
		if ( i == 3 || i == 5 || i == 7 || i == 9 )
			outString += "-";
	}
	return outString;
}

function uuidStringToBuffer(data) {
	var outBuffer = new Buffer(16);
	var index = 0;
	for ( var i = 0; i < data.length; i += 2 ) {
		if ( i == 8 || i == 12 || i == 16 || i == 20 )
			continue;

		var c = data[i] + data[i + 1];
		outBuffer[index++] = parseInt(c, 16);		
	}
	return outBuffer;
}

router.use(function (req, res, next) {   
	next();
});

router.use(bodyParser.json());

router.get("/", function(req, res) {  
	var ftx = function(req, res) {    
        dbDataManager.selectURLList((result, error) => {
            if ( error ) {              
                return res.status(500).end(JSON.stringify({}));              
            }

            var outString = "[";
            for ( var i = 0; i < result.length; i++ ) {
                var currentItem = result[i];		
                outString += JSON.stringify(currentItem);
                if ( i < result.length - 1 ) {
                    outString += ",";
                }			
            }
            outString += "]";
            
            res.status(200).end(outString);
        });
    };

    ftx(req, res);
});

router.get("/:id", function(req, res) {
    var ftx = function(req, res) {
        if (!req.params.id) {
            res.status(400).end(JSON.stringify({}));
        }

        var id = req.params.id;      
        dbDataManager.selectURL(id, (result, error) => {
            if ( error ) {
                return res.status(500).end(JSON.stringify({}));
            }
        
            res.status(200).end(JSON.stringify(result));	
        });
    };

    ftx(req, res);
});

router.post("/", function(req, res) {
    var ftx = function(req, res) {
        if (!req.body) {
            res.status(400).end(JSON.stringify({}));
        }
        
        var data = req.body;

        if ( !data.id ) {
            data.id = uuidV4();
        }

		var buffer = uuidStringToBuffer(data.id);
		var decimal = buffer.readUIntBE(0, 2);
		var shortURL = base62.encode(decimal);
		data.shortURL = shortURL;
      
        dbDataManager.insertURL(data, (result, error) => {
            if ( error ) {
               return res.status(500).end(JSON.stringify({}));
            }

            res.status(201).end(JSON.stringify({
				id: data.id,
				shortURL: "http://127.0.0.1:9000/" + data.shortURL
			}));
        });
    };

    ftx(req, res);
});

router.put("/:id", function(req, res) {
    var ftx = function(req, res) {
        if (!req.body) {
            res.status(400).end(JSON.stringify({}));
        }	
        
        var data = req.body;
        var id = req.params.id;     
        dbDataManager.updateURL(id, data, (result, error) => {
            if ( error ) {
                return res.status(500).end(JSON.stringify({}));
            }
            
            res.status(200).end(JSON.stringify({}));
        });
    };

    ftx(req, res);
});

router.delete("/:id", function(req, res) {
    var ftx = function(req, res) {
        if (!req.params.id) {
            res.status(400).end(JSON.stringify({}));
        }
        
        var id = req.params.id;      
        dbDataManager.deleteURL(id, (result, error) => {
            if ( error ) {
               return res.status(500).end(JSON.stringify({}));
            }

            res.status(200).end(JSON.stringify({}));
        });
    };

    ftx(req, res);
});

module.exports = router;
