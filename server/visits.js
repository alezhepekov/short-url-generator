const express = require("express");
const router = express.Router();
const bodyParser = require("body-parser");
const uuidV4 = require("uuid/v4");

const dbdatamanager = require("./dbdatamanager");
var dbDataManager = new dbdatamanager.DBDataManager();

router.use(function (req, res, next) {   
	next();
});

router.use(bodyParser.json());

router.get("/", function(req, res) {  
	var ftx = function(req, res) {    
        dbDataManager.selectVisitList((result, error) => {
            if ( error ) {              
                return res.status(500).end(JSON.stringify({}));              
            }

            var outString = "[";
            for ( var i = 0; i < result.length; i++ ) {
                var currentItem = result[i];		
                outString += JSON.stringify(currentItem);
                if ( i < result.length - 1 ) {
                    outString += ",";
                }			
            }
            outString += "]";
            
            res.status(200).end(outString);
        });
    };

    ftx(req, res);
});

router.get("/:id", function(req, res) {
    var ftx = function(req, res) {
        if (!req.params.id) {
            res.status(400).end(JSON.stringify({}));
        }

        var id = req.params.id;      
        dbDataManager.selectVisit(id, (result, error) => {
            if ( error ) {
                return res.status(500).end(JSON.stringify({}));
            }
        
            res.status(200).end(JSON.stringify(result));	
        });
    };

    ftx(req, res);
});

router.post("/", function(req, res) {
    var ftx = function(req, res) {
        if (!req.body) {
            res.status(400).end(JSON.stringify({}));
        }
        
        var data = req.body;

        if ( !data.id ) {
            data.id = uuidV4();
        }            
      
        dbDataManager.insertVisit(data, (result, error) => {
            if ( error ) {
               return res.status(500).end(JSON.stringify({}));
            }

            res.status(201).end(JSON.stringify({id: data.id}));
        });
    };

    ftx(req, res);
});

router.put("/:id", function(req, res) {
    var ftx = function(req, res) {
        if (!req.body) {
            res.status(400).end(JSON.stringify({}));
        }	
        
        var data = req.body;
        var id = req.params.id;     
        dbDataManager.updateVisit(id, data, (result, error) => {
            if ( error ) {
                return res.status(500).end(JSON.stringify({}));
            }
            
            res.status(200).end(JSON.stringify({}));
        });
    };

    ftx(req, res);
});

router.delete("/:id", function(req, res) {
    var ftx = function(req, res) {
        if (!req.params.id) {
            res.status(400).end(JSON.stringify({}));
        }
        
        var id = req.params.id;      
        dbDataManager.deleteVisit(id, (result, error) => {
            if ( error ) {
               return res.status(500).end(JSON.stringify({}));
            }

            res.status(200).end(JSON.stringify({}));
        });
    };

    ftx(req, res);
});

module.exports = router;
