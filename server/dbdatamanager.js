const config = require("./config");
var mysql = require("mysql");

function DBDataManager() {
    var pool = mysql.createPool({
        host: config.DB.host,
        user: config.DB.user,
        password: config.DB.password,
        database: config.DB.database
    });

    this.pool = pool;   
}

/* urls */

DBDataManager.prototype.selectURLList = function(cb) {
    this.pool.getConnection(function(error, connection) {
        if (error) {          
            return cb && cb(null, error);
        }

        var sql = "SELECT * FROM `urls`";
        connection.query(sql, (error, result, fields) => {
            connection.release();
            if (error) {          
                return cb && cb(null, error);
            }            

            cb && cb(result);
        });
    });   
}

DBDataManager.prototype.selectURL = function(id, cb) {
    this.pool.getConnection(function(error, connection) {
        if (error) {          
            return cb && cb(null, error);
        }

        var sql = "SELECT * FROM `urls` WHERE `id` = ?";
        var args = [id];
        connection.query(sql, args, (error, result, fields) => {
            connection.release();
            if (error) {          
                return cb && cb(null, error);
            }            

            cb && cb(result);
        });
    });  
}

DBDataManager.prototype.selectURLByShortURL = function(shortURL, cb) {
    this.pool.getConnection(function(error, connection) {
        if (error) {          
            return cb && cb(null, error);
        }

        var sql = "SELECT * FROM `urls` WHERE `shorturl` = ?";
        var args = [shortURL];
        connection.query(sql, args, (error, result, fields) => {
            connection.release();
            if (error) {          
                return cb && cb(null, error);
            }            

            cb && cb(result);
        });
    });  
}

DBDataManager.prototype.insertURL = function(data, cb) {
    this.pool.getConnection(function(error, connection) {
        if (error) {          
            return cb && cb(null, error);
        }

        var sql = "INSERT INTO `urls`(`id`, `url`, `shorturl`) VALUES (?, ?, ?)";
        var args = [data.id, data.url, data.shortURL];
        connection.query(sql, args, (error, result, fields) => {
            connection.release();
            if (error) {          
                return cb && cb(null, error);
            }            

            cb && cb(result);
        });
    }); 
}

DBDataManager.prototype.updateURL = function(id, data, cb) {
    // TODO
    cb && cb();
}

DBDataManager.prototype.deleteURL = function(id, cb) {
    // TODO
    cb && cb();
}

/* visits */

DBDataManager.prototype.selectVisitList = function(cb) {
    this.pool.getConnection(function(error, connection) {
        if (error) {          
            return cb && cb(null, error);
        }

        var sql = "SELECT * FROM `visits`";
        connection.query(sql, (error, result, fields) => {
            connection.release();
            if (error) {          
                return cb && cb(null, error);
            }            

            cb && cb(result);
        });
    });   
}

DBDataManager.prototype.selectVisit = function(id, cb) {
    this.pool.getConnection(function(error, connection) {
        if (error) {          
            return cb && cb(null, error);
        }

        var sql = "SELECT * FROM `visits` WHERE `id` = ?";
        var args = [id];
        connection.query(sql, args, (error, result, fields) => {
            connection.release();
            if (error) {          
                return cb && cb(null, error);
            }            

            cb && cb(result);
        });
    });  
}

DBDataManager.prototype.insertVisit = function(data, cb) {
    this.pool.getConnection(function(error, connection) {
        if (error) {          
            return cb && cb(null, error);
        }

        var sql = "INSERT INTO `visits`(`id`, `url`, `ip`, `data`) VALUES (?, ?, ?, ?)";
        var args = [data.id, data.url, data.ip, data.data];
        connection.query(sql, args, (error, result, fields) => {
            connection.release();
            if (error) {          
                return cb && cb(null, error);
            }            

            cb && cb(result);
        });
    }); 
}

DBDataManager.prototype.updateVisit = function(id, data, cb) {
    // TODO
    cb && cb();
}

DBDataManager.prototype.deleteVisit = function(id, cb) {
    // TODO
    cb && cb();
}

module.exports.DBDataManager = DBDataManager;